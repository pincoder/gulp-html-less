var gulp = require('gulp'),
  less = require('gulp-less'),
  watch = require('gulp-watch'),
  concat = require('gulp-concat'),
  plumber = require('gulp-plumber'),
  htmlmin = require('gulp-htmlmin'),
  uglyfly = require('gulp-uglyfly'),
  imagemin = require('gulp-imagemin'),
  concatCss = require('gulp-concat-css'),
  minifyCSS = require('gulp-minify-css'),
  pngquant = require('imagemin-pngquant'),
  spritesmith = require('gulp.spritesmith'),
  server = require('gulp-server-livereload'),
  autoprefixer = require('gulp-autoprefixer');

// Htmlmin
gulp.task('htmlmin', function() {
  return gulp.src('app/pages/*.html')
    .pipe(htmlmin({collapseWhitespace: true}))
    .pipe(gulp.dest('dist'))
});

// less
gulp.task('less', function () {
  return gulp.src('app/styles/*.less')
    .pipe(less())
    .pipe(plumber())
    .pipe(autoprefixer({
    browsers: ['last 15 versions'],
    cascade: false
    }))
    .pipe(gulp.dest('app/styles/css/'));
});

// sprite
gulp.task('sprite', function() {
    var spriteData = 
    gulp.src('app/images/*.png')
      .pipe(spritesmith({
          imgName: 'sprite.png',
          cssName: 'sprite.less',
          cssFormat: 'less',
          algorithm: 'binary-tree',
          padding: 0,
          cssVarMap: function(sprite) {
            sprite.name = 's-' + sprite.name
          }
      }));

    spriteData.img
    .pipe(imagemin({
      optimizationLevel: 7,
      progressive: true,
      svgoPlugins: [{removeViewBox: false}],
      use: [pngquant({
         quality: '65-100', speed: 4
      })]
    }))
    .pipe(gulp.dest('dist/img/sprite/'));
    spriteData.css.pipe(gulp.dest('app/sprite/'));
});

// Concat JS
gulp.task('concatjs', function() {
  return gulp.src('app/scripts/*.js')
    .pipe(concat('common.min.js'))
    .pipe(uglyfly())
    .pipe(gulp.dest('dist/scripts'));
});

// concat css
gulp.task('concatCss', function () {
  gulp.src('app/styles/css/*.css')
    .pipe(concatCss("bundle.css"))
    .pipe(minifyCSS())
    .pipe(gulp.dest('dist/styles/'));
});

// server
gulp.task('server', function() {
  gulp.src('dist')
    .pipe(server({
      livereload: true,
      open: true,
      port: 3000
    }))
});

// Watch
gulp.task('watch', function () {
    gulp.watch('app/styles/*.less', ['less']);
    gulp.watch('app/images/*.png', ['sprite']);
    gulp.watch('app/pages/*.html', ['htmlmin']);
    gulp.watch('app/scripts/*.js', ['concatjs']);
    gulp.watch('app/styles/css/*.css', ['concatCss']);
});

gulp.task('default',['server', 'watch', 'sprite']);